from tkinter import EXCEPTION
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect, QueryDict
import json
import time
import requests
from .models import Server, VirtualMachine
from backend.settings import *
import libvirt
from apscheduler.schedulers.background import BackgroundScheduler
from django_apscheduler.jobstores import DjangoJobStore, register_events, register_job
from xml.etree import ElementTree
import os

# key: vm_id, value: vm
vm_map = {}
# job scheduler
scheduler = BackgroundScheduler()
scheduler.add_jobstore(DjangoJobStore(), 'default')
register_events(scheduler)
scheduler.start()

'''
initialize influxdb, the connection and job schedulers
'''
def init_db(request):
    try:
        # init influxdb
        data = {'q': 'show databases'}
        rsp = requests.post(QUERY_URL, data=data)
        rsp = eval(str(rsp.content, encoding='utf-8'))
        dbs = rsp['results'][0]['series'][0]['values']
        create_database = True
        for db in dbs:
            if db[0] == INFLUXDB_NAME:
                create_database = False
        if create_database:
            # create database if not exist
            data = {'q': f'create database {INFLUXDB_NAME}'}
            rsp = requests.post(QUERY_URL, data=data)
            # create RP
            data = {'q': f'create retention policy "one_month" on \"{INFLUXDB_NAME}\" duration 30d replication 1 default'}
            rsp = requests.post(QUERY_URL, data=data)
        # connect servers
        virtual_machines = VirtualMachine.objects.all()
        for virtual_machine in virtual_machines:
            server = Server.objects.get(pk=virtual_machine.server_id.server_id)
            conn = libvirt.open('qemu+ssh://%s@%s/system' % (server.server_username, server.server_ip))
            vm_map[virtual_machine.vm_id] = conn.lookupByName(virtual_machine.vm_name)
            # start monitoring existing virtual machines and saving data into influxdb every 5 seconds
            scheduler.add_job(db_job, 'interval', id=str(virtual_machine.vm_id), seconds=5, args=[virtual_machine.vm_id], replace_existing=True)
        return JsonResponse({'msg': 'SUCCESS'})
    except Exception as e:
        return JsonResponse({'msg': str(e)})


'''
return all servers' information
'''
def get_servers(request):
    servers = Server.objects.all()
    server_list = []
    for server in servers:
        server_list.append({
          'server_id': server.server_id,
          'server_ip': server.server_ip,
          'server_username': server.server_username,
          'server_password': server.server_password
        })

    return JsonResponse({
      'msg': 'SUCCESS',
      'servers': server_list,
    })

'''
add one server's information to backend db
'''
def add_server(request):
    server_ip = request.GET.get('server_ip')
    server_username = request.GET.get('server_username')
    server_password = request.GET.get('server_password')
    try:
        Server.objects.get(server_ip=server_ip)
    except Server.DoesNotExist as e:
        # ssh connection
        # os.system(f'ssh-copy-id -i ~/.ssh/id_rsa.pub {server_username}@{server_ip} -f')

        server = Server(server_ip=server_ip, server_username=server_username, server_password=server_password)
        server.save()
        ret_server = Server.objects.get(server_ip=server_ip)
        return JsonResponse({'msg':'SUCCESS', 'server_id': ret_server.pk})

    return JsonResponse({'msg':'SERVER EXISTS'})

'''
given the server_id, delete related server and virtual machines
'''
def delete_server(request):
    server_id = request.GET.get('server_id')
    try:
        vms = VirtualMachine.objects.filter(server_id=server_id)
        for vm in vms:
            vm_map.pop(vm.pk)
            scheduler.remove_job(str(vm.pk))
        Server.objects.get(pk=server_id).delete()
        return JsonResponse({'msg': "SUCCESS"})
    except Exception as e:
        return JsonResponse({'msg': str(e)})

'''
given the server_id, return the server's vms' information
'''
def get_vms(request):
    server_id = request.GET.get('server_id')
    vms = VirtualMachine.objects.filter(server_id=server_id)
    vm_list = []
    for vm in vms:
        current_state = vm_map[vm.pk].state()
        # if state changes
        if current_state==libvirt.VIR_DOMAIN_SHUTDOWN and vm.status==1:
            vm.status=0
            vm.save()
            scheduler.pause_job(vm.pk)
        elif current_state==libvirt.VIR_DOMAIN_RUNNING and vm.status==0:
            vm.status=1
            vm.save()
            scheduler.resume_job(vm.pk)
        
        vm_list.append({
            'vm_id': vm.vm_id,
            'vm_name': vm.vm_name,
            'vm_ip' : vm.vm_ip, 
            'vm_username' : vm.vm_username,
            'vm_status': vm.vm_status,
            'server_id' : vm.server_id.server_id
        })
    
    return JsonResponse({
      'msg': 'SUCCESS',
      'vms': vm_list
    })


'''
given vm's information, add vm to db, start monitoring and create CQ
'''
def add_vm(request):
    vm_name = request.GET.get('vm_name')
    vm_ip = request.GET.get('vm_ip')
    vm_username = request.GET.get('vm_username')
    server_id = request.GET.get('server_id')
    try:
        server = Server.objects.get(pk=server_id)
        server_username = server.server_username
        server_ip = server.server_ip
    except Server.DoesNotExist as e:
        return JsonResponse({'msg': 'SERVER NOT EXIST'})
    
    try:
        conn = libvirt.open('qemu+ssh://%s@%s/system'%(server_username,server_ip))
        # if exists?
        vm = conn.lookupByName(vm_name)
        status = 1
        if vm.state() == libvirt.VIR_DOMAIN_SHUTDOWN:
            status=0
        vm_data = VirtualMachine(vm_name=vm_name, vm_ip=vm_ip, vm_username=vm_username, server_id=server, vm_status=status)
        vm_data.save()
        vm_id = VirtualMachine.objects.get(vm_name=vm_name).pk
        # create CQ
        data = {'q': 'create continuous query "cq_vm_%d" on "%s" \
                begin select mean("cpu_occupation") as "cpu_occupation",\
                mean("disk_occupation") as "disk_occupation",\
                mean("mem_occupation") as "mem_occupation",\
                mean("net_rate") as "net_rate"\
                into "avg_vm_%d" from "vm_%d" group by time(30m) end'%(vm_id,INFLUXDB_NAME,vm_id,vm_id)}
        rsp = requests.post(QUERY_URL, data=data)
        vm_map[vm_id] = vm
        # scheduler
        scheduler.add_job(db_job, 'interval', id=str(vm_id), seconds=5, args=[vm_id], replace_existing=True)
        return JsonResponse({'msg': 'SUCCESS'})
    except Exception as e:
        return JsonResponse({'msg': str(e)})
        

'''
given the vm_id, delete the virtual machine
'''
def delete_vm(request):
    try:
        vm_id = request.GET.get('vm_id')
        vm_map.pop(int(vm_id))
        scheduler.remove_job(vm_id)
        VirtualMachine.objects.get(pk=vm_id).delete()
        return JsonResponse({'msg':'SUCCESS'})
    except Exception as e:
        return JsonResponse({'msg': str(e)})


'''
given the vm_id, get the statistics
'''
def get_metrics(request):
  try:
    vm_id = request.GET.get('vm_id')
    data_num = request.GET.get('data_num')
    data = {
        'q': 'select * from "one_month"."vm_%d" order by time desc limit %d'%(int(vm_id), int(data_num))
    }
    rsp = requests.post(QUERY_URL, data=data)
    rsp = eval(str(rsp.content,encoding='utf-8'))
    res = rsp['results'][0]['series'][0]['values']
    metric_list = []
    for i in range(len(res)):
      ret = {}   
      ret['time'] = res[i][0]      
      ret['cpu_occupation'] = res[i][1] 
      ret['disk_occupation'] = res[i][2] 
      ret['mem_occupation'] = res[i][3] 
      ret['net_rate'] = res[i][4]
      metric_list.append(ret)
      
    return JsonResponse({'metric_list':metric_list})
  except Exception as e:
    return JsonResponse({'msg': str(e)})




def db_job(vm_id):
    try:
        vm = vm_map[vm_id]

        # cpu occupation
        t1 = time.time()
        cpu_time1 = int(vm.info()[4])
        time.sleep(2)
        t2 = time.time()
        cpu_time2 = int(vm.info()[4])
        cpu_num = int(vm.info()[3])
        cpu_occupation = (cpu_time2-cpu_time1)/((t2-t1)*cpu_num*1e9)*100

        # net rate
        tree = ElementTree.fromstring(vm.XMLDesc())
        ifaces = tree.findall('devices/interface/target')
        bytes1=0
        bytes2=0
        t1 = time.time()
        for i in ifaces:
            iface = i.get('dev')
            ifaceinfo = vm.interfaceStats(iface)
            bytes1 += ifaceinfo[0]+ifaceinfo[4]
        time.sleep(2)
        t2 = time.time()
        for i in ifaces:
            iface = i.get('dev')
            ifaceinfo = vm.interfaceStats(iface)
            bytes2 += ifaceinfo[0]+ifaceinfo[4]
        # rx_bytes + tx_bytes   bytes/s
        net_rate = (bytes2-bytes1)/(t2-t1)

        # disk occupation
        devices = tree.findall('devices/disk/target')
        disk_cnt = 0
        disk_occupation = 0.0
        for d in devices:
            device = d.get('dev')
            try:
                devinfo = vm.blockInfo(device)
                total_disk=float(devinfo[0])
                use_disk=float(devinfo[1])
                disk_occupation*=disk_cnt
                disk_occupation+=use_disk/total_disk*100
                disk_cnt+=1
                disk_occupation/=disk_cnt
            except Exception as e:
                continue
        
        
        # memory occupation
        vm.setMemoryStatsPeriod(10)
        meminfo = vm.memoryStats()
        free_mem = float(meminfo['unused'])
        total_mem = float(meminfo['available'])
        mem_occupation = ((total_mem-free_mem) / total_mem)*100

        data='vm_%d cpu_occupation=%f,disk_occupation=%f,mem_occupation=%f,net_rate=%f'%(vm_id,cpu_occupation,disk_occupation,mem_occupation,net_rate)
        rsp = requests.post(WRITE_URL,data=data)

    except Exception as e:
        scheduler.remove_job(str(vm_id))


