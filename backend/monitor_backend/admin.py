from django.contrib import admin
from monitor_backend import models

admin.site.register(models.Server)
admin.site.register(models.VirtualMachine)