from django.db import models

class Server(models.Model):
    server_id = models.AutoField(primary_key=True,db_index=True)
    server_ip = models.CharField(max_length=15)
    server_username = models.CharField(max_length=255)
    server_password = models.CharField(max_length=255)

class VirtualMachine(models.Model):
    vm_id = models.AutoField(primary_key=True, db_index=True)
    vm_name = models.CharField(max_length=255)
    vm_ip = models.CharField(max_length=15)
    vm_username = models.CharField(max_length=255)
    vm_status = models.IntegerField()
    server_id = models.ForeignKey('Server',to_field='server_id',on_delete=models.CASCADE)