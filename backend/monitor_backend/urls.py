from django.urls import path
from . import views

urlpatterns = [
    path('initdb', views.init_db),
    path('getservers', views.get_servers),
    path('addserver', views.add_server),
    path('deleteserver', views.delete_server),
    path('getvms',views.get_vms),
    path('addvm',views.add_vm),
    path('deletevm', views.delete_vm),
    path('getmetrics', views.get_metrics)
]
