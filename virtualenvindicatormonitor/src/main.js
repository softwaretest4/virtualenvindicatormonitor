import Vue from 'vue'
import App from './App.vue'
import './plugins/element.js'
import * as echarts from 'echarts'
import './iconfont.js'
import IconSvg from "./components/IconSvg"

Vue.config.productionTip = false
Vue.component('icon-svg', IconSvg)
Vue.prototype.$echarts = echarts
new Vue({
  render: h => h(App),
}).$mount('#app')
