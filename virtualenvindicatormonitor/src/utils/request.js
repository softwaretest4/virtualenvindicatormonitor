import axios from "axios"

const server = axios.create({
    baseURL: 'http://localhost:8000/monitor_backend',
    timeout: 6000,
    headers: {}
})

server.interceptors.request.use(function (config) {
    console.log('请求拦截器已经执行')
    return config
}, function (error) {
    return Promise.reject(error)
})

export default server
