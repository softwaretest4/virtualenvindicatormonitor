import request from "../utils/request.js"

export function initDB (data) {
    return request({
        url: '/initdb',
        method: 'get',
        params: data
    })
}

export function getServers (data) {
    return request({
        url: '/getservers',
        method: 'get',
        params: data
    })
}

export function addServer (data) {
    return request({
        url: '/addserver',
        method: 'get',
        params: data
    })
}

export function deleteServer (data) {
    return request({
        url: '/deleteserver',
        method: 'get',
        params: data
    })
}

export function getVirtualMachine (data) {
    return request({
        url: '/getvms',
        method: 'get',
        params: data
    })
}

export function addVirtualMachine (data) {
    return request({
        url: '/addvm',
        method: 'get',
        params: data
    })
}

export function deleteVirtualMachine (data) {
    return request({
        url: '/deletevm',
        method: 'get',
        params: data
    })
}

export function getMetric (data) {
    return request({
        url: '/getmetrics',
        method: 'get',
        params: data
    })
}