import Vue from 'vue'
import {
    Button,
    Divider,
    Popover,
    Card,
    Tabs,
    TabPane,
    Dialog,
    Form,
    Input,
    FormItem,
    Select,
    Option,
    MessageBox,
    Message,
    Empty,
    Notification
} from 'element-ui'

Vue.use(Button)
Vue.use(Divider)
Vue.use(Popover)
Vue.use(Card)
Vue.use(Tabs)
Vue.use(TabPane)
Vue.use(Dialog)
Vue.use(Form)
Vue.use(Input)
Vue.use(FormItem)
Vue.use(Select)
Vue.use((Option))
Vue.use(Empty)
Vue.prototype.$confirm = MessageBox.confirm
Vue.prototype.$message = Message
Vue.prototype.$notify = Notification
