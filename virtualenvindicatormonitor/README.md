# virtualenvindicatormonitor

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Lints and fixes files
```
npm run lint
```

## 项目结构
- src

     - assets：存放图片

     - components：存放组件，本次项目希望主要以组件为主

     - plugins

          - element.js：该文件负责`element-ui`的组件注册，而不将代码放在`main.js`里

     - App.vue：网站的主页面，尽量以此页面为主

     - iconfont.js：本次`icon`采用`svg`形式，在`iconfont`官网将`icon`加入项目，并将项目的`symbol`代码复制到该文件

     - main.js
