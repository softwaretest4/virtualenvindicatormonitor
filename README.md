# VirtualEnvIndicatorMonitor
- `virtualenvindicatormonitor`文件夹：vue项目文件夹


## Getting started

### 运行Vue项目

```
cd virtualenvindicatormonitor
npm run serve
```

### 后端
```
cd backend
docker-compose down
docker-compose up
```
1. 前端直接发送get即可
2. 使用docker时，在settings->resources->network的Docker subnet处填入自己WLAN网段号，如我的填入"10.22.192.0/18"
3. 需要自己在backend容器里面将id_rsa.pub文件内容添加到服务器端的 ~/.ssh/authorized_keys中，我在views.py尝试使用`# os.system(f'ssh-copy-id -i ~/.ssh/id_rsa.pub {server_username}@{server_ip} -f')` 但是报错，目前没找到合适的办法。。。

## 使用`Gitlab`规范

- 在`Repository`-`Branches`页面，根据自己所要完成的功能命名分支，一次完成的功能不应过多

- 回到本地`Git`仓库

    ```
    # 更新本地对于远程分支的数据
    git fetch 
    # 将远程分支拉到本地
    git checkout -b 分支名 origin/分支名 （两个“分支名”应该保持一致）
    # 即可开始工作
    ```

- 本地工作完成后

    ```
    git add .
    git commit -m "描述你所做的工作"
    # 与main分支同步，解决冲突
    git pull origin main --rebase
    # 查看产生冲突的文件
    git status
    # 解决冲突后
    git add .
    git rebase --continue
    # 所有冲突解决完后
    git push
    ```

- 回到`Gitlab`，发起`merge request`请求

